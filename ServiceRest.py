'''
Un service REST qui fournit un emplacement de prise de rechargement disponible à proximité de coordonnées GPS passées en paramètre
'''
### imports
import requests
from werkzeug.datastructures import _omd_bucket

from app import ServiceWeb

class ServiceRest:
    rayonBorne = 5000 #en mètres
    
    def __init__(self):
        super().__init__()
    
    ### fonction    
    def getBorne(coordonnees):
        latitude = str(coordonnees[0])
        longitude = str(coordonnees[1])
        
        url = 'https://opendata.reseaux-energies.fr/api/records/1.0/search/?dataset=bornes-irve&q=&lang=fr&rows=1'
        url += "&geofilter.distance=" + latitude + "%2C" + longitude
        url += "%2C" + str(ServiceRest.rayonBorne)
        
        rqt = requests.get(url)
        
        infosBorne = rqt.json() 
        parameters = infosBorne["parameters"]
        coordonees = parameters["geofilter.distance"]
        coordonees = coordonees[0]
        coordonees = coordonees.split(",")
        coordonees = [coordonees[0], coordonees[1]]
        adresse = ServiceRest.getAddress(coordonees[0], coordonees[1])
        return adresse

    def getAddress(latitude,longitude):
        url = "https://geocode.xyz/"
        requete = url + latitude + "," + longitude + "?json=1" + "&region=FR" + "&auth=123172621061502201835x42344 "
        rqt = requests.get(requete)
        
        infosAddress = rqt.json()
        
        infosAddress
        rue = infosAddress["staddress"]
        ville = infosAddress["city"]
        osmtags = infosAddress["osmtags"]
        postcode = osmtags["ref_INSEE"]
        
        address = "Rue: " + str(rue) + ", " + str(ville) + ", " + str(postcode)
        
        return address