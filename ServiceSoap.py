'''
Un service SOAP à développer pour calculer le temps de parcours en fonction du nombre de kilomètres à parcourir et en tenant compte du temps de chargement
'''
### imports
from flask import Flask, jsonify, request
from math import ceil
import datetime

soap = Flask(__name__)

@soap.route('/', methods=['GET', 'POST'])
def index():
    if(request.method == 'POST'):
        some_json = request.get.json()
        return jsonify({"Envoi": some_json}), 201
    else:
        return jsonify({"about": "Hello World!"})

@soap.route('/tempsdeparcours/<int:nbkilometres>+<int:tempsdechargement>+<int:autonomie>', methods=['GET'])
def get_tempsdeparcours(nbkilometres, tempsdechargement, autonomie):
    nombreArrets = nbkilometres/autonomie
    if nombreArrets < 1:
        tps_charge_total = 0
    elif nombreArrets >= 1:
        tps_charge_total = ceil(nombreArrets) * tempsdechargement
    
    vitesse_moyenne = 110 #en km/h
    v_moy_kmparseconde = int(vitesse_moyenne)/3600
    
    temps_trajet = nbkilometres/v_moy_kmparseconde
    
    temps_trajet_total = temps_trajet + tps_charge_total
    
    duree = str(datetime.timedelta(seconds=temps_trajet_total))
    TotalDuration=str(duree).split('.', 2)[0]
    
    return jsonify({'temps': TotalDuration})    

if __name__ == "__main__":
    soap.run(host='127.0.0.1', port=8000)