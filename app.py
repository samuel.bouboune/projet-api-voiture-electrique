'''
Application
'''
###
from flask import Flask, render_template, redirect, url_for, request, flash
from math import ceil
from ServiceRest import *
import ServiceSoap
import time, folium
from mysql import connector

import os, requests, json, datetime

### Déclaration des variables
PEOPLE_FOLDER = os.path.join('static', 'img')

### Service Web
app = Flask(__name__)
app.debug = True
app.config['UPLOAD_FOLDER'] = PEOPLE_FOLDER

@app.route('/', methods=['GET', 'POST'])
@app.route('/index.html', methods=['GET', 'POST'])
    
def home():
    full_filename = os.path.join(app.config['UPLOAD_FOLDER'], '/static/img/favicon.png')
    source = ""
    dest = ""
    if request.method == "POST":
        ### réinitialisation des variables de la classe ServiceWeb
        ServiceWeb.reinitVar
        
        ### Récupération des infos de l'IHM
        source = request.form.get("source")
        dest = request.form.get("dest") 
        modeleVehicule = request.form.get("vehicule")
        
        ### gestion erreur input vide IHM 
        if (source == "" or dest == ""):
            return render_template("index.html", favicon = full_filename)
        
        ### Récupération des informations pour le trajet
        autonomie = ServiceWeb.getAutonomieVehicule(modeleVehicule)
        latLonDepart = ServiceWeb.setSource(source)
        latLonArrivee = ServiceWeb.setDest(dest)
        
        ### Récupération de la distance
        infosTrajet = ServiceWeb.getTrajet(ServiceWeb.localisationLonLat[0], ServiceWeb.localisationLonLat[1])
        distance = int(infosTrajet[1])
        
        ### liste des arrets en fonction de l'autonomie et la distance
        tableCoordArrets = ServiceWeb.calculCoordArretRecharge(latLonDepart[0], latLonDepart[1], latLonArrivee[0], latLonArrivee[1], autonomie, distance)
         
        ### tableau des bornes
        n=1
        for arret in tableCoordArrets:
            localisationBorne = ServiceRest.getBorne(arret)
            ServiceWeb.listeDesArrets.append(localisationBorne)
            n = n+1
        
        ### Appel api SOAP pour calcul durée
        tempsCharge = ServiceWeb.getTempsCharge(modeleVehicule)
        
        url = 'http://127.0.0.1:8000/tempsdeparcours/'
        requete = url + str(distance) + "+" + str(tempsCharge) + "+" + str(autonomie)
        rqt = requests.get(requete)
        
        ### Traitement des data recues de l'api
        infosAddress = rqt.json() 
        tempsDeParcoursComplet = infosAddress["temps"]
        
        ### Création de la carte
        carte = folium.Map(width=1500,height=500,location=latLonDepart, zoom_level=0)
        folium.Marker(latLonDepart).add_to(carte)
        for arret in tableCoordArrets:
            folium.Marker(arret, popup="Station de chargement", icon=folium.Icon(color="green")).add_to(carte)
        folium.Marker(latLonArrivee).add_to(carte)
        
        return render_template("index.html", favicon = full_filename, carte = carte._repr_html_(), modeleVehicule = modeleVehicule, autonomie = autonomie, source = source, dest = dest, tempsDeChargement=ServiceWeb.getTime(tempsCharge), bornes = ServiceWeb.listeDesArrets, tempsDeParcours = tempsDeParcoursComplet, distance = distance)

    return render_template("index.html", favicon = full_filename)

class ServiceWeb:
    localisationLatLon = []
    localisationLonLat = []
    infosTrajet = []
    listeDesArrets = []
    listeVehicules = []
    
    def __init__(self):
        pass

    def reinitVar():
        ServiceWeb.localisationLatLon = []
        ServiceWeb.localisationLonLat = []
        ServiceWeb.infosTrajet = []
        ServiceWeb.listeDesArrets = []

    def setSource(src):
        # print('\n *** Adresse source ***\n')
        url = 'https://api-adresse.data.gouv.fr/search/?q='
        requete = url + src
        requete += "&postcode="
        rqt = requests.get(requete)
        
        infosAddress = rqt.json() 
        
        features = infosAddress["features"]
        features_list = features[0]
        geometry = features_list["geometry"]
        coordonees = geometry["coordinates"]
        
        latitude = str(coordonees[1])
        longitude = str(coordonees[0])
        
        coordSource = latitude + "," + longitude
        ServiceWeb.localisationLatLon.append(coordSource)
        
        coordSource = longitude + "," + latitude
        ServiceWeb.localisationLonLat.append(coordSource)
        
        return [latitude, longitude]

    def setDest(dst):
        # print('\n*** Adresse destination ***\n')
        url = 'https://api-adresse.data.gouv.fr/search/?q='
        requete = url + dst
        requete += "&postcode="
        rqt = requests.get(requete)
        
        infosAddress = rqt.json() 
        
        features = infosAddress["features"]
        features_list = features[0]
        geometry = features_list["geometry"]
        coordonees = geometry["coordinates"]
        
        latitude = str(coordonees[1])
        longitude = str(coordonees[0])
        
        coordSource = latitude + "," + longitude
        ServiceWeb.localisationLatLon.append(coordSource)
        
        coordSource = longitude + "," + latitude
        ServiceWeb.localisationLonLat.append(coordSource)
        
        return [latitude, longitude]

    def getSourceLoc():
        source = []
        coordSource = ServiceWeb.localisationLatLon[0]
        # print("\n*************************************** INFO SOURCE ***************************************\n" + coordSource)
        return coordSource

    def getDestLoc():
        dest = []
        coordDest = ServiceWeb.localisationLatLon[1]
        # print("\n***************************************  INFO DEST  ***************************************\n" + coordDest)
        return coordDest

    def getTrajet(source, dest):
        
        url = "http://router.project-osrm.org/route/v1/car/"
        url += source + ";" + dest #+ "?overview=false"
        # then you load the response using the json libray
        # by default you get only one alternative so you access 0-th element of the `routes`
        r = requests.get(url)
        routes = json.loads(r.content)
        trajet = routes.get("routes")[0]
        
        time = trajet["duration"]
        totalDuration = ServiceWeb.getTime(time)

        distanceM = int( trajet["distance"] )
        distanceKm = int(distanceM * 0.001)
        
        infosTrajet = [str(totalDuration), str(distanceKm)]
        
        return infosTrajet

    def getTime(tempsEnSecondes):
        duree = str(datetime.timedelta(seconds=tempsEnSecondes))
        TotalDuration=str(duree).split('.', 2)[0]
        return TotalDuration

    def calculCoordArretRecharge(latDepart, lonDepart, latArrivee, lonArrive, autonomieVoiture, distance):
        nombreArrets = ceil( distance/autonomieVoiture )
        
        total = []
        dest = []
        
        if nombreArrets < 1:
            total = []
            return total
        
        else:
            diffLatitude = (float(latArrivee) - float(latDepart)) / nombreArrets
            diffLongitude = (float(lonArrive) - float(lonDepart)) / nombreArrets

            latitudeArret = float(latDepart)
            longitudeArret = float(lonDepart)
            
            for arret in range(nombreArrets - 1):
                dest = []
                
                latitudeArret = latitudeArret + diffLatitude
                longitudeArret = longitudeArret + diffLongitude
                
                dest.append(latitudeArret)
                dest.append(longitudeArret)
                
                total.append(dest)
                
            return total

    def getDatabaseInfo():
        connection_params = {
            'host': "sql11.freesqldatabase.com",
            'user': "sql11454080",
            'password': "YrrYbqwtWZ",
            'database': "sql11454080",
        }

        request = "select * from vehicules"

        with connector.connect(**connection_params) as db :
            with db.cursor() as c:
                c.execute(request)
                garage = c.fetchall()
                for voiture in garage:
                    vehicule = {"marque":voiture[0], "modele":voiture[1], "autonomie":voiture[2], "tempsDeChargement":voiture[3]}
                    ServiceWeb.listeVehicules.append(vehicule)

    def getAutonomieVehicule(modele):
        modeleVoiture = str(modele)
        for vehicule in ServiceWeb.listeVehicules:
            if modeleVoiture == str(vehicule["modele"]):
                autonomie = vehicule["autonomie"]
                return autonomie
            
    def getTempsCharge(modele):
        modeleVoiture = str(modele)
        for vehicule in ServiceWeb.listeVehicules:
            if modeleVoiture == str(vehicule["modele"]):
                tempsDeChargement = vehicule["tempsDeChargement"]
                return tempsDeChargement

### Main
if __name__ == '__main__':
    ServiceWeb.getDatabaseInfo()
    app.run(host="127.0.0.1", port=5000)